# LSA Sling

An Apache Sling based project for LSA

Build and install to a running instance of Sling
`mvn clean install -P autoInstallBundle`

Review the running [documentation](https://docs.google.com/document/d/1mktFkuqQ4ePLlzozDP2rAQ4nHzwu9SZLUaGFMr3Gh5k/edit#) for LSA Sling and LSA Peregrine projects.
